
import org.daisy.pipeline.junit.AbstractXSpecAndXProcSpecTest;
import static org.daisy.pipeline.pax.exam.Options.mavenBundle;
import static org.daisy.pipeline.pax.exam.Options.thisPlatform;
import org.ops4j.pax.exam.Configuration;
import static org.ops4j.pax.exam.CoreOptions.composite;
import static org.ops4j.pax.exam.CoreOptions.options;
import org.ops4j.pax.exam.Option;

public class DediconTest extends AbstractXSpecAndXProcSpecTest {

    @Override
    protected String[] testDependencies() {
        return new String[]{
            brailleModule("common-utils"),
            brailleModule("css-utils"),
            brailleModule("liblouis-utils"),
            "org.daisy.pipeline.modules.braille:liblouis-utils:jar:" + thisPlatform() + ":?",
            brailleModule("libhyphen-utils"),
            "org.daisy.pipeline.modules.braille:libhyphen-utils:jar:" + thisPlatform() + ":?",
            brailleModule("dotify-utils"),
            brailleModule("pef-utils"),
            brailleModule("dtbook-to-pef"),
            "org.codehaus.woodstox:stax2-api:?",
            "org.daisy.pipeline:logging-appender:?",
            "org.daisy.pipeline:logging-activator:?",
        };
    }
    
    @Override
    @Configuration
    public Option[] config() {
        return options(
            // FIXME: BrailleUtils needs older version of jing
            mavenBundle("org.daisy.libs:jing:20120724.0.0"),
            composite(super.config()));
    }
}
