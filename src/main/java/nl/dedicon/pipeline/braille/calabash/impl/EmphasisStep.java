package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.io.ReadablePipe;
import com.xmlcalabash.io.WritablePipe;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import nl.dedicon.pipeline.braille.step.Emphasis;
import nl.dedicon.pipeline.braille.step.Utils;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * XProc step for emphasis
 * 
 * <p>In general, there is only one emphasis sign, viz. (456). This is how
 * Liblouis is configured and that should not be changed.
 * However, for Educational stuff it is important to distinguish between bold,
 * italics and underline. This is done through the class attribute:
 * <ul>
 * <li>class="emph-bxx-brl": bold</li>
 * <li>class="emph-xix-brl": italics</li>
 * <li>class="emph-xxu-brl": underline</li>
 * </ul>
 * </p>
 * 
 * @author Paul Rambags
 */
public class EmphasisStep extends DefaultStep implements XProcStep {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EmptyBackCoverStep.class);

    private ReadablePipe source = null;
    private WritablePipe result = null;
    
    private EmphasisStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void setInput(String port, ReadablePipe pipe) {
        source = pipe;
    }

    @Override
    public void setOutput(String port, WritablePipe pipe) {
        result = pipe;
    }

    @Override
    public void reset() {
        source.resetReader();
        result.resetWriter();
    }

    @Override
    public void run() throws SaxonApiException {
        
        super.run();

        try {

            XdmNode pef = source.read();

            // convert the immutable XdmNode to a modifiable Document
            Document document = Utils.convertToDocument(pef);

            Emphasis.apply(document);

            XdmNode newBook = Utils.convertToXdmNode(document, runtime.getProcessor(), true);
            result.write(newBook);
            result.close();
            
        } catch (Exception e) {

            LOGGER.error("dedicon:emphasis failed", e);
            throw new XProcException(step.getNode(), e);

        }
    }

    @Component(
            name = "dedicon:emphasis",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}emphasis"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new EmphasisStep(runtime, step);
        }

    }
}
