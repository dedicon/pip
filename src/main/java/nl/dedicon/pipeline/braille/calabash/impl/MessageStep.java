package nl.dedicon.pipeline.braille.calabash.impl;

import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.io.ReadablePipe;
import com.xmlcalabash.io.WritablePipe;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import net.sf.saxon.s9api.XdmNode;
import nl.dedicon.pipeline.braille.step.MessageContainer;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;

public class MessageStep extends DefaultStep implements XProcStep {

    private static final QName _message = new QName("message");
    private static final QName _severity = new QName("severity");
    private static final QName _reset = new QName("reset");

    private static final MessageContainer messageContainer = new MessageContainer();
    
    private ReadablePipe source = null;
    private WritablePipe result = null;
    
    private MessageStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void setInput(String port, ReadablePipe pipe) {
        source = pipe;
    }

    @Override
    public void setOutput(String port, WritablePipe pipe) {
        result = pipe;
    }

    @Override
    public void reset() {
        source.resetReader();
        result.resetWriter();
    }

    @Override
    public void run() throws SaxonApiException {
        super.run();

        XdmNode node = source.read();
        if (node != null) {
            result.write(node);
            result.close();
        }

        String severity = getOption(_severity, "INFO");
        String message = getOption(_message, (String)null);
        boolean reset = getOption(_reset, false);
        
        if (reset) {
            messageContainer.reset();
        }
        
        message = messageContainer.getElapsedTime().concat(" ").concat(message);
        
        if ("WARN".equals(severity)) {
            warning(this.step.getNode(), message);
        } else if ("DEBUG".equals(severity)) {
            // DefaultStep#fine() removed in ndw/xmlcalabash1@ce9b07d
            this.runtime.getMessageListener().fine(this, this.step.getNode(), message);
        } else {
            info(this.step.getNode(), message);
        }
    }

    @Component(
        name = "dedicon:message",
        service = {XProcStepProvider.class},
        property = {"type:String={http://www.dedicon.nl}message"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new MessageStep(runtime, step);
        }
    }
}
