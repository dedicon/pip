package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.io.ReadablePipe;
import com.xmlcalabash.io.WritablePipe;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import nl.dedicon.pipeline.braille.step.JacketManager;
import nl.dedicon.pipeline.braille.step.Utils;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * XProc step for the jacket (the cover)
 * 
 * <level1> elements with class "flap" or "jacket_copy":
 *  - will get the jacket-header, an existing header will become a paragraph (<p>)
 *  - will be moved to the front
 * 
 * @author Paul Rambags
 */
public class JacketStep extends DefaultStep implements XProcStep {

    private static final Logger logger = LoggerFactory.getLogger(JacketStep.class);

    private static final QName _jacket_header = new QName("jacket-header");
    private static final QName _move_print_cover = new QName("move-print-cover");

    private ReadablePipe source = null;
    private WritablePipe result = null;
    
    private JacketStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void setInput(String port, ReadablePipe pipe) {
        source = pipe;
    }

    @Override
    public void setOutput(String port, WritablePipe pipe) {
        result = pipe;
    }

    @Override
    public void reset() {
        source.resetReader();
        result.resetWriter();
    }

    @Override
    public void run() throws SaxonApiException {
        
        super.run();

        try {

            XdmNode book = source.read();
            String header = getOption(_jacket_header, "");
            String movePrintCover = getOption(_move_print_cover, "");

            // convert the immutable XdmNode to a modifiable Document
            Document document = Utils.convertToDocument(book);

            JacketManager jacketManager = new JacketManager();
            
            jacketManager.collect(document);
            jacketManager.setHeader(header);
            jacketManager.move(movePrintCover);

            XdmNode newBook = Utils.convertToXdmNode(document, runtime.getProcessor(), true);
            result.write(newBook);
            result.close();
            
        } catch (Exception e) {

            logger.error("dedicon:jacket failed", e);
            throw new XProcException(step.getNode(), e);

        }
    }

    @Component(
            name = "dedicon:jacket",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}jacket"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new JacketStep(runtime, step);
        }

    }
}
