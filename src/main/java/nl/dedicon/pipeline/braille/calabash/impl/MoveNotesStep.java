package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.io.ReadablePipe;
import com.xmlcalabash.io.WritablePipe;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XdmNode;
import nl.dedicon.pipeline.braille.step.NotesMover;
import nl.dedicon.pipeline.braille.step.Utils;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * XProc step for moving notes
 * 
 * Notes are moved right after the parent of the corresponding noteref element
 * 
 * @author Paul Rambags
 */
public class MoveNotesStep extends DefaultStep implements XProcStep {

    private static final Logger logger = LoggerFactory.getLogger(MoveNotesStep.class);

    private ReadablePipe source = null;
    private WritablePipe result = null;
    
    private MoveNotesStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void setInput(String port, ReadablePipe pipe) {
        source = pipe;
    }

    @Override
    public void setOutput(String port, WritablePipe pipe) {
        result = pipe;
    }

    @Override
    public void reset() {
        source.resetReader();
        result.resetWriter();
    }

    @Override
    public void run() throws SaxonApiException {
        
        super.run();

        try {

            XdmNode book = source.read();

            // convert the immutable XdmNode to a modifiable Document
            Document document = Utils.convertToDocument(book);

            NotesMover notesMover = new NotesMover();
            notesMover.moveNotes(document);

            XdmNode newBook = Utils.convertToXdmNode(document, runtime.getProcessor(), true);

            result.write(newBook);
            result.close();
            
        } catch (Exception e) {

            logger.error("dedicon:move-notes failed", e);
            throw new XProcException(step.getNode(), e);

        }
    }

    @Component(
            name = "dedicon:move-notes",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}move-notes"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new MoveNotesStep(runtime, step);
        }

    }
}
