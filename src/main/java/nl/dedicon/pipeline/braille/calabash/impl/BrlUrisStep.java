package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.io.ReadablePipe;
import com.xmlcalabash.io.WritablePipe;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import net.sf.saxon.s9api.XQueryCompiler;
import net.sf.saxon.s9api.XQueryEvaluator;
import net.sf.saxon.s9api.XQueryExecutable;
import net.sf.saxon.s9api.XdmAtomicValue;
import net.sf.saxon.s9api.XdmNode;
import net.sf.saxon.s9api.XdmValue;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.daisy.dotify.api.embosser.FileFormat;
import static org.daisy.pipeline.braille.common.Provider.util.dispatch;
import static org.daisy.pipeline.braille.common.Provider.util.memoize;
import org.daisy.pipeline.braille.common.Provider.util.MemoizingProvider;
import org.daisy.pipeline.braille.common.Query;
import static org.daisy.pipeline.braille.common.Query.util.mutableQuery;
import static org.daisy.pipeline.braille.common.Query.util.query;
import org.daisy.pipeline.braille.pef.FileFormatProvider;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XProc step that returns the URIs of the generated BRL files
 * 
 * @author Paul Rambags
 */
public class BrlUrisStep extends DefaultStep implements XProcStep {

    private static final String DEDICON_NAMESPACE = "http://www.dedicon.nl";

    private static final Logger logger = LoggerFactory.getLogger(MetadataStep.class);

    private static final QName _xquery = new QName("xquery");
    private static final QName _brf_dir_href = new QName("brf-dir-href");
    private static final QName _brf_file_extension = new QName("brf-file-extension");
    private static final QName _brf_file_format = new QName("brf-file-format");
    private static final QName _brf_name_pattern = new QName("brf-name-pattern");
    private static final QName _brf_number_width = new QName("brf-number-width");

    private final MemoizingProvider<Query,FileFormat> fileFormatProvider;

    private ReadablePipe source = null;
    private WritablePipe result = null;
    
    private BrlUrisStep(XProcRuntime runtime, XAtomicStep step, MemoizingProvider<Query,FileFormat> fileFormatProvider) {
        super(runtime, step);
        this.fileFormatProvider = fileFormatProvider;
    }

    @Override
    public void setInput(String port, ReadablePipe pipe) {
        source = pipe;
    }

    @Override
    public void setOutput(String port, WritablePipe pipe) {
        result = pipe;
    }

    @Override
    public void reset() {
        source.resetReader();
        result.resetWriter();
    }

    @Override
    public void run() throws SaxonApiException {
        super.run();

        try {
            
            XdmNode pef = source.read();
            
            String xquery = getOption(_xquery, "");
            String brfDirHref = getOption(_brf_dir_href, "");
            String brfFileFormat = getOption(_brf_file_format, "");
            String brfNamePattern = getOption(_brf_name_pattern, "");
            int brfNumberWidth = getOption(_brf_number_width, 0);

            String brfFileExtension = getFileExtension(brfFileFormat);
            InputStream query = new URL(xquery).openConnection().getInputStream();
            
            XQueryCompiler xqCompiler = runtime.getProcessor().newXQueryCompiler();

            XQueryExecutable xqExecutable = xqCompiler.compile(query);
            XQueryEvaluator xqEvaluator = xqExecutable.load();

            xqEvaluator.setSource(pef.asSource());
            xqEvaluator.setExternalVariable(_brf_dir_href, new XdmAtomicValue(brfDirHref));
            xqEvaluator.setExternalVariable(_brf_name_pattern, new XdmAtomicValue(brfNamePattern));
            xqEvaluator.setExternalVariable(_brf_number_width, new XdmAtomicValue(brfNumberWidth));
            xqEvaluator.setExternalVariable(_brf_file_extension, new XdmAtomicValue(brfFileExtension));

            XdmValue xqResult = xqEvaluator.evaluate();
            
            for (XdmValue xqValue : xqResult) {
                if (xqValue instanceof XdmNode) {
                    result.write((XdmNode)xqValue);
                }
            }
            
            result.close();
            
        } catch (Exception e) {

            logger.error("dedicon:brl-uris failed", e);
            throw new XProcException(step.getNode(), e);

        }
    }

    private String getFileExtension (String fileFormatQuery) {
        Query.MutableQuery q = mutableQuery(query(fileFormatQuery));
        Iterable<FileFormat> fileFormats = fileFormatProvider.get(q);
        String fileExtension = "";
        for (FileFormat fileFormat : fileFormats) {
            fileExtension = fileFormat.getFileExtension();
            break;
        }
        return fileExtension;
    }
    
    @Component(
            name = "dedicon:brl-uris",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}brl-uris"}
    )
    public static class Provider implements XProcStepProvider {

        private List<FileFormatProvider> fileFormatProviders = new ArrayList<>();
        private MemoizingProvider<Query,FileFormat> fileFormatProvider = memoize(dispatch(fileFormatProviders));

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new BrlUrisStep(runtime, step, fileFormatProvider);
        }

        @Reference(
                name = "FileFormatProvider",
                unbind = "unbindFileFormatProvider",
                service = FileFormatProvider.class,
                cardinality = ReferenceCardinality.MULTIPLE,
                policy = ReferencePolicy.DYNAMIC
        )
        protected void bindFileFormatProvider(FileFormatProvider provider) {
                fileFormatProviders.add(provider);
        }

        protected void unbindFileFormatProvider(FileFormatProvider provider) {
                fileFormatProviders.remove(provider);
                this.fileFormatProvider.invalidateCache();
        }
    }
}
