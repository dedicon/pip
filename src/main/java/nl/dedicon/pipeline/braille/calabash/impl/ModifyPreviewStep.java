package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import net.sf.saxon.s9api.Processor;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import nl.dedicon.pipeline.braille.step.BrailleToText;
import nl.dedicon.pipeline.braille.step.Utils;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;

/**
 * XProc step for modifying the preview HTML
 * 
 * The pipeline can generate a preview for the generated PEF,
 * which is an HTML with a braille view and a text view.
 * This step modifies the text view, such that digits,
 * capitals and certain symbols are displayed correctly
 * according to specifications of Dedicon.
 * 
 * If the structure of the generated HTML preview changes,
 * this step may have to be revised.
 * 
 * This step is structured as follows:
 * 
 * * The preview HTML is read as a string
 * * The header (everything before "<html ") is removed
 *   and is processed as XML
 * * The processed XML is converted to a string,
 *   all XML end tags are replaced by HTML end tags,
 *   and the header is prepended
 * * The result overwrites the original preview
 *   
 * @author Paul Rambags
 */
public class ModifyPreviewStep extends DefaultStep implements XProcStep {

    private static final Logger logger = LoggerFactory.getLogger(ModifyPreviewStep.class);

    private static final QName _preview_uri = new QName("preview-uri");

    private ModifyPreviewStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void reset() {
    }

    @Override
    public void run() throws SaxonApiException {
        super.run();

        try {

            String previewUri = getOption(_preview_uri, "");
            
            Processor processor = runtime.getProcessor();

            Path previewPath = new File(new URI(previewUri)).toPath();
            String sourceHtml = new String(Files.readAllBytes(previewPath), "UTF-8");
            int htmlStartIndex = sourceHtml.indexOf("<html ");
            String header = sourceHtml.substring(0, htmlStartIndex);
            String sourceXml = sourceHtml.substring(htmlStartIndex);
            Document document = Utils.convertToDocument(sourceXml);

            new BrailleToText().convert(document);
            
            String resultXml = Utils.toString(document, processor.newDocumentBuilder());
            String resultHtml = header.concat(Utils.convertXmlEndtagsToHtmlEndtags(resultXml));
            Files.write(previewPath, resultHtml.getBytes("UTF-8"));
            
        } catch (Exception e) {

            logger.error("dedicon:modify-preview failed", e);
            throw new XProcException(step.getNode(), e);

        }
    }

    @Component(
            name = "dedicon:modify-preview",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}modify-preview"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new ModifyPreviewStep(runtime, step);
        }
    }
}
