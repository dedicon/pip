package nl.dedicon.pipeline.braille.calabash.impl;

import com.xmlcalabash.core.XProcException;
import com.xmlcalabash.core.XProcRuntime;
import com.xmlcalabash.library.DefaultStep;
import com.xmlcalabash.runtime.XAtomicStep;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import net.sf.saxon.s9api.QName;
import net.sf.saxon.s9api.SaxonApiException;
import org.apache.commons.lang3.StringUtils;
import org.daisy.common.xproc.calabash.XProcStep;
import org.daisy.common.xproc.calabash.XProcStepProvider;
import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * XProc step for inserting a newline in BRL files
 * 
 * @author Paul Rambags
 */
public class BrlMustStartWithNewlineStep extends DefaultStep implements XProcStep {

    private static final Logger logger = LoggerFactory.getLogger(BrlMustStartWithNewlineStep.class);

    private static final QName _brl_uri = new QName("brl-uri");

    private BrlMustStartWithNewlineStep(XProcRuntime runtime, XAtomicStep step) {
        super(runtime, step);
    }

    @Override
    public void reset() {
    }

    @Override
    public void run() throws SaxonApiException {
        super.run();

        try {
            String brlUri = getOption(_brl_uri, "");
            if (StringUtils.isNotBlank(brlUri)) {
                
                try {

                    Path path = new File(new URI(brlUri)).toPath();
                    // The path refers to a CP850 encoded file
                    // A CR + LF (hex 0D 0A) is inserted if the file does not already start with it
                    byte[] bytes = Files.readAllBytes(path);
                    if (bytes.length > 1 && bytes[0] != 0xD && bytes[1] != 0xA) {
                        byte[] newLine = new byte[]{ 0xD, 0xA };
                        Files.write(path, newLine);
                        Files.write(path, bytes, StandardOpenOption.APPEND);
                    }

                } catch (URISyntaxException|IOException e) {
                    logger.error(String.format("Failed to insert a newline in file %s due to %s", brlUri, e.getMessage()));
                    throw e;
                }
            }
        } catch (Exception e) {
            logger.error("dedicon:brl-must-start-with-newline failed", e);
            throw new XProcException(step.getNode(), e);
        }
    }

    @Component(
            name = "dedicon:brl-must-start-with-newline",
            service = {XProcStepProvider.class},
            property = {"type:String={http://www.dedicon.nl}brl-must-start-with-newline"}
    )
    public static class Provider implements XProcStepProvider {

        @Override
        public XProcStep newStep(XProcRuntime runtime, XAtomicStep step) {
            return new BrlMustStartWithNewlineStep(runtime, step);
        }
    }
}
