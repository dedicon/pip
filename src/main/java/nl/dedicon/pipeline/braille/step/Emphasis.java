package nl.dedicon.pipeline.braille.step;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import static org.w3c.dom.Node.ELEMENT_NODE;
import static org.w3c.dom.Node.TEXT_NODE;

/**
 * Processing of emphasized text
 * 
 * @author Paul Rambags
 */
public class Emphasis {

    // emph-bxx-brl, bold -> \u2838 ⠸ (456)
    // emph-xix-brl, italic -> \u2810 ⠐ (5)
    // emph-xxu-brl, underline -> \u2808 ⠈ (4)
    public static final String[] EMPHASIS_CLASSES = {
        "emph-bxx-brl", "bold", "emph-xix-brl", "italic", "emph-xxu-brl", "underline"
    };
    // the corresponding Braille emphasis symbols
    public static final char[] EMPHASIS_SYMBOLS = {
        '\u2838', '\u2838', '\u2810', '\u2810', '\u2808', '\u2808'
    };
    
    public static final char HYPHEN = '-';
    
    // recovery sign ⠠ (6): must be inserted when an emphasis ends in the middle of a word.
    public static final char RECOVERY_SIGN = '\u2820';
    
    // a recovery sign is not needed if the end of emphasis is immediately
    // followed by one of these punctuation characters
    public static final char[] PUNCTUATION_CHARS = {
        '.', '\u2832',  // \u2832 = ⠲
        ',', '\u2802',  // \u2802 = ⠂
        '-', '\u2824',  // \u2824 = ⠤
        ';', '\u2806',  // \u2806 = ⠆
        ':', '\u2812',  // \u2812 = ⠒
        '?', '\u2822',  // \u2822 = ⠢
        '(', '\u2826',  // \u2826 = ⠦
        ')', '\u2834',  // \u2834 = ⠴
        '!', '\u2816',  // \u2816 = ⠖
        '"', '\u2836',  // \u2836 = ⠶
        '\'','\u2804',  // \u2804 = ⠄
        '\u2820'        // recovery sign
    };
    
    private static final String CLASS = "class";
    
    /**
     * Apply emphasis based on the class attribute.
     * 
     * <p>This is to override default Liblouis functionality. By default, Liblouis
     * has only one symbol for emphasis, viz. (456). This is according to the
     * Braille standard and is correct for general literature. However, for
     * Educational stuff we need to distinguish among bold, italics and underline.
     * <ul>
     * <li>class="emph-bxx-brl" or class="bold": bold</li>
     * <li>class="emph-xix-brl" or class="italic": italics</li>
     * <li>class="emph-xxu-brl" or class="underline": underline</li>
     * </ul>
     * </p>
     * 
     * @param document 
     */
    public static void apply(Document document) {
        apply(document.getDocumentElement());
    }
    
    private static void apply(Element element) {
        final String classValue = element.getAttribute(CLASS);
        if (classValue != null && classValue.length() > 0) {
            for (int i = 0; i < EMPHASIS_CLASSES.length; ++i) {
                if (EMPHASIS_CLASSES[i].equalsIgnoreCase(classValue)) {
                    addEmphasis(element, EMPHASIS_SYMBOLS[i]);
                    break;
                }
            }
        }
        // recurse through child elements
        Node childNode = element.getFirstChild();
        while(childNode != null) {
            if (childNode.getNodeType() == ELEMENT_NODE) {
                apply((Element)childNode);
            }
            childNode = childNode.getNextSibling();
        }
    }
    
    private static void addEmphasis(Element element, char emphasisSymbol) {
        Node childNode = element.getFirstChild();
        boolean hasEmphasis = false;
        while(childNode != null) {
            if (childNode.getNodeType() == TEXT_NODE) {
                if (Emphasis.addEmphasis(childNode, emphasisSymbol)) {
                    hasEmphasis = true;
                }
            }
            childNode = childNode.getNextSibling();
        }
        if (hasEmphasis) {
            insertRecoverySignIfNeeded(element);
        }
    }
    
    /**
     * Add emphasis to a text node
     * 
     * @param textNode text node
     * @param emphasisSymbol emphasis symbol
     * @return whether emphasis was added
     */
    private static boolean addEmphasis(Node textNode, char emphasisSymbol) {
        final String textContent = textNode.getTextContent();
        if (textContent == null || textContent.length() == 0) {
            return false;
        }
        
        final String[] words = textContent.trim().split(" ");
        for (int i = 0; i < words.length; ++i) {
            words[i] = insertEmphasisAfterHyphen(words[i], emphasisSymbol);
        }
        final StringBuffer newTextContent = new StringBuffer();
        boolean atStart = true;
        if (words.length > 3) {
            
            // add two emphasis symbols at the beginning and one before the last word
            newTextContent.append(emphasisSymbol).append(emphasisSymbol);
            int i = 0;
            while (i < words.length - 1) {
                if (atStart) {
                    atStart = false;
                } else {
                    newTextContent.append(' ');
                }
                newTextContent.append(words[i]);
                ++i;
            }
            newTextContent.append(' ').append(emphasisSymbol).append(words[i]);
            
        } else {
            
            // add an emphasis symbol before each word
            for (String word : words) {
                if (atStart) {
                    atStart = false;
                } else {
                    newTextContent.append(' ');
                }
                newTextContent.append(emphasisSymbol).append(word);
            }
            
        }
        textNode.setTextContent(newTextContent.toString());
        return true;
    }
    
    private static String insertEmphasisAfterHyphen(String word, char emphasisSymbol) {
        if (word.length() < 2) {
            return word;
        }
        // do not insert an emphasis symbol if the hyphen occurs at the end of the word
        final String wordExceptLastChar = word.substring(0, word.length() - 1);
        final String lastChar = word.substring(word.length() - 1, word.length());
        return StringUtils.replace(
                wordExceptLastChar,
                Character.toString(HYPHEN),
                Character.toString(HYPHEN) + Character.toString(emphasisSymbol)
        ) + lastChar;
    }
    
    private static void insertRecoverySignIfNeeded(Element element) {
        // a recovery sign must be added if the next sibling (or the next
        // sibling of the parent, if there is no next sibling, etc.)
        // is a text element and it does not start with white space or a hyphen
        Node parent = element;
        while (parent != null && parent.getNextSibling() == null) {
            parent = parent.getParentNode();
        }
        if (parent == null) {
            return;
        }
        final Node nextSibling = parent.getNextSibling();
        // nextSibling != null
        if (nextSibling.getNodeType() != TEXT_NODE) {
            return;
        }
        final String textContent = nextSibling.getTextContent();
        if (textContent == null || textContent.length() == 0) {
            return;
        }
        final char firstChar = textContent.charAt(0);
        if (("" + firstChar).trim().isEmpty()) {
            // the first character is white space
            return;
        }
        
        // Do not add a recovery sign after a punctuation sign
        // Note: This is incorrect in the case of an abbreviation. However,
        // it is impossible to determine whether a period marks the end of a
        // sentence or whether it belongs to an abbreviation.
        for (char c : PUNCTUATION_CHARS) {
            if (firstChar == c) {
                return;
            }
        }
        
        // insert recovery sign
        nextSibling.setTextContent(RECOVERY_SIGN + textContent);
    }

}
