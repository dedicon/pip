package nl.dedicon.pipeline.braille.step;

/**
 * Capital Mode
 * 
 * @author Paul Rambags
 */
public enum CapitalMode {
    lowercase,
    letter,
    word,
    permanent
}
