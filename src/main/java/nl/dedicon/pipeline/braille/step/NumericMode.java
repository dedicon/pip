package nl.dedicon.pipeline.braille.step;

/**
 * Numeric Mode
 * 
 * @author Paul Rambags
 */
public enum NumericMode {
    start,
    yes,
    decimalSign,
    moneyZeros,
    no
}
