package nl.dedicon.pipeline.braille.step;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Move notes right after the parent node of their corresponding noteref
 * 
 * @author Paul Rambags
 */
public class JacketManager {
    
    public static final String[] JACKET_CLASSES = { "flap", "jacket_copy", "jacketcopy" };
    
    private static final String CLASS = "class";
    private static final String LEVEL1 = "level1";
    private static final String H1 = "h1";
    private static final String P = "p";
    
    private final List<Element> jackets = new ArrayList<>();      // Element(level1)
    
    /**
     * Collect all jackets (there should be at most one)
     * 
     * @param document Document
     */
    public void collect(Document document) {
        String namespace = document.getDocumentElement().getNamespaceURI();
        NodeList level1List = document.getElementsByTagNameNS(namespace, LEVEL1);
        for (int i = 0; i < level1List.getLength(); ++i) {
            Element level1 = (Element)level1List.item(i);
            String classValue = level1.getAttribute(CLASS);
            // wait for version 3.7 of commons-lang3
            // if (StringUtils.equalsAny(classValue, JACKET_CLASSES)) {
            if (Arrays.asList(JACKET_CLASSES).contains(classValue)) {
                jackets.add(level1);
            }
        }
    }

    /**
     * set the jacket-header
     * an existing header will become a paragraph
     * 
     * @param header Jacket header
     */
    public void setHeader(String header) {
        if (StringUtils.isBlank(header)) {
            return;
        }

        for (Element jacket : jackets) {
            Node headerNode = Utils.getChild(jacket, H1);
            if (headerNode != null) {
                Utils.renameNode(headerNode, P);
            }
            Node newHeaderNode = Utils.addChildBefore(jacket, jacket.getFirstChild(), H1);
            newHeaderNode.setTextContent(header);
        }
    }

    /**
     * move jackets to the front
     */
    public void move(String moveLocation) {
        if (jackets.isEmpty()) {
            return;
        }
        
        final Location location = Location.valueOf(moveLocation.toUpperCase());
        if (location == null) {
            throw new IllegalArgumentException("Cannot move jackets to location '" + moveLocation + "'");
        }

        Document document = jackets.get(0).getOwnerDocument();
        Element dtbook = document.getDocumentElement();
        Node book = Utils.getChild(dtbook, "book");

        if (book == null) {
            return;
        }
        
        switch (location) {
            case FRONT: insertFront(jackets, book); break;
            case REAR: insertRear(jackets, book); break;
        }
        
    }
    
    private void insertFront(List<Element> jackets, Node book) {
        
        Node frontMatter = Utils.getChild(book, "frontmatter");
        if (frontMatter == null) {
            frontMatter = Utils.addChild(book, "frontmatter");
        }

        Node refChild = frontMatter.getFirstChild();
        for (Element jacket : jackets) {
            frontMatter.insertBefore(jacket, refChild);
            refChild = jacket.getNextSibling();
        }
        
    }
    
    private void insertRear(List<Element> jackets, Node book) {
        
        Node rearMatter = Utils.getChild(book, "rearmatter");
        if (rearMatter == null) {
            rearMatter = Utils.addChild(book, "rearmatter");
        }

        for (Element jacket : jackets) {
            rearMatter.appendChild(jacket);
        }
        
    }
    
    private enum Location {
        FRONT,
        REAR
    }
}
