package nl.dedicon.pipeline.braille.step;

import java.time.Duration;
import java.time.LocalDateTime;

public class MessageContainer {

    private LocalDateTime dateTime = LocalDateTime.now();
    
    public void reset() {
        dateTime = LocalDateTime.now();
    }
    
    public String getElapsedTime() {
        Duration duration = Duration.between(dateTime, LocalDateTime.now());
        long millis = duration.toMillis();
        long seconds = millis/1000;
        long millisPart = millis % 1000;
        long minutes = seconds/60;
        long secondsPart = seconds % 60;
        return String.format("%d:%02d.%03d", minutes, secondsPart, millisPart);
    }

}
