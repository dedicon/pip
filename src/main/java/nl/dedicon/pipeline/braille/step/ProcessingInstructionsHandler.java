package nl.dedicon.pipeline.braille.step;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import static java.util.stream.Collectors.toSet;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import static org.w3c.dom.Node.ELEMENT_NODE;
import static org.w3c.dom.Node.PROCESSING_INSTRUCTION_NODE;

/**
 * Handles processing instructions
 * 
 * @author Paul Rambags
 */
public class ProcessingInstructionsHandler {
    
    private final static String DEDICON_NAMESPACE = "http://www.dedicon.nl";
    private final static String DEDICON_PREFIX = "dedicon";
    
    public static void replaceProcessingInstructions(Document document) {
        addDediconNamespace(document);
        List<Node> processingInstructions = findProcessingInstructions(document);
        Set<Node> volumeBreakProcessingInstructions = processingInstructions
                .stream()
                .filter(processingInstruction -> "VolumeBreak".equals(processingInstruction.getNodeName()))
                .collect(toSet());
        Set<Element> volumeBreakNextElements = volumeBreakProcessingInstructions
                .stream()
                .map(ProcessingInstructionsHandler::nextElement)
                .filter(Objects::nonNull)
                .collect(toSet());
        volumeBreakNextElements.forEach(ProcessingInstructionsHandler::setVolumeBreakBefore);
        Utils.removeNodes(volumeBreakProcessingInstructions);
    }
    
    private static void addDediconNamespace(Document document) {
        document.getDocumentElement().setAttribute("xmlns" + ":" + DEDICON_PREFIX, DEDICON_NAMESPACE);
    }
    
    private static List<Node> findProcessingInstructions(Node node) {
        List<Node> processingInstructions = new ArrayList<>();
        if (node.getNodeType() == PROCESSING_INSTRUCTION_NODE) {
            processingInstructions.add(node);
        }
        for (Node child = node.getFirstChild(); child != null; child = child.getNextSibling()) {
            processingInstructions.addAll(findProcessingInstructions(child));
        }
        return processingInstructions;
    }
    
    private static Element nextElement(Node node) {
        while (node != null) {
            Node nextElement = node.getNextSibling();
            while(nextElement != null && nextElement.getNodeType() != ELEMENT_NODE) {
               nextElement = nextElement.getNextSibling();
            }
            if (nextElement != null) {
                return (Element)nextElement;
            }
            // if there is no Element sibling, try the parent node
            node = node.getParentNode();
        }
        // no next element found - do not apply a volume break
        return null;
    }
    
    private static void setVolumeBreakBefore(Element element) {
        element.setAttributeNS(DEDICON_NAMESPACE, DEDICON_PREFIX + ":" + "break-before", "volume");
    }
    
}
