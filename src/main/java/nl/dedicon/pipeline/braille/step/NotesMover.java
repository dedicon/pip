package nl.dedicon.pipeline.braille.step;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Move notes right after the parent node of their corresponding noteref
 * 
 * @author Paul Rambags
 */
public class NotesMover {
    
    private static final String NOTE = "note";
    private static final String NOTEREF = "noteref";
    private static final String ID = "id";
    private static final String IDREF = "idref";
    
    public void moveNotes(Document document) {
        String namespace = document.getDocumentElement().getNamespaceURI();
        NodeList noteRefList = document.getElementsByTagNameNS(namespace, NOTEREF);
        Map<String, Element> idRef2noteRefMap = getIdRef2noteRefMap(noteRefList);
        NodeList noteList = document.getElementsByTagNameNS(namespace, NOTE);
        List<Element> notes = new ArrayList<>();
        for (int i = noteList.getLength() - 1; i >= 0; --i) {
            notes.add((Element)noteList.item(i));
        }
        notes.forEach(note -> moveNote(note, idRef2noteRefMap));
    }
    
    private static Map<String, Element> getIdRef2noteRefMap(NodeList noteRefList) {
        Map<String, Element> idRef2noteRefMap = new HashMap<>();
        for (int i = 0; i < noteRefList.getLength(); ++i) {
            Element noteRef = (Element)noteRefList.item(i);
            String idRef = noteRef.getAttribute(IDREF);
            if (idRef != null) {
                idRef = idRef.replaceFirst("^#", "");
                if (idRef.length() > 0) {
                    idRef2noteRefMap.put(idRef, noteRef);
                }
            }
        }
        return idRef2noteRefMap;
    }
    
    private static void moveNote(Element note, Map<String, Element> idRef2noteRefMap) {
        String id = note.getAttribute(ID);
        if (id == null || id.length() == 0) {
            return;
        }
        Element noteRef = idRef2noteRefMap.get(id);
        if (noteRef == null) {
            // note without noteref
            return;
        }
        Node parent = noteRef.getParentNode();
        if (parent == null) {
            return;
        }
        Node grandParent = parent.getParentNode();
        if (grandParent == null) {
            return;
        }

        grandParent.insertBefore(note, parent.getNextSibling());
    }
    
}
