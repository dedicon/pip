package nl.dedicon.pipeline.braille.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Paul Rambags
 */
@Data
public class Book {

    private List<Volume> volumes = new ArrayList<>();
    
}
