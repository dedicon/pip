package nl.dedicon.pipeline.braille.model;

import java.util.ArrayList;
import java.util.List;
import lombok.Data;

/**
 *
 * @author Paul Rambags
 */
@Data
public class Volume {
    
    private Integer firstPrintPageNumber;
    private Integer lastPrintPageNumber;

    private Integer firstPageNumber;
    private Integer lastPageNumber;
    
    private Boolean duplex;
    
    private List<Section> sections = new ArrayList<>();
    
}
