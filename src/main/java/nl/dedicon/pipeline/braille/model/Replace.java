package nl.dedicon.pipeline.braille.model;

/**
 *
 * @author Paul Rambags
 */
public class Replace {
    
    private Symbol parent;
    private Context context;
    private String braille;
    private Boolean brailleAddLeadingSpace;
    private Boolean brailleRemoveLeadingSpace;
    private Boolean brailleAddTrailingSpace;
    private Boolean brailleRemoveTrailingSpace;
    private String brailleOpen;
    private String brailleClose;
    private String description;

    public Symbol getParent() {
        return parent;
    }

    public void setParent(Symbol parent) {
        this.parent = parent;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public String getBraille() {
        return braille;
    }

    public void setBraille(String braille) {
        this.braille = braille;
    }

    public Boolean getBrailleAddLeadingSpace() {
        return brailleAddLeadingSpace;
    }

    public void setBrailleAddLeadingSpace(Boolean brailleAddLeadingSpace) {
        this.brailleAddLeadingSpace = brailleAddLeadingSpace;
    }

    public Boolean getBrailleRemoveLeadingSpace() {
        return brailleRemoveLeadingSpace;
    }

    public void setBrailleRemoveLeadingSpace(Boolean brailleRemoveLeadingSpace) {
        this.brailleRemoveLeadingSpace = brailleRemoveLeadingSpace;
    }

    public Boolean getBrailleAddTrailingSpace() {
        return brailleAddTrailingSpace;
    }

    public void setBrailleAddTrailingSpace(Boolean brailleAddTrailingSpace) {
        this.brailleAddTrailingSpace = brailleAddTrailingSpace;
    }

    public Boolean getBrailleRemoveTrailingSpace() {
        return brailleRemoveTrailingSpace;
    }

    public void setBrailleRemoveTrailingSpace(Boolean brailleRemoveTrailingSpace) {
        this.brailleRemoveTrailingSpace = brailleRemoveTrailingSpace;
    }

    public String getBrailleOpen() {
        return brailleOpen;
    }

    public void setBrailleOpen(String brailleOpen) {
        this.brailleOpen = brailleOpen;
    }

    public String getBrailleClose() {
        return brailleClose;
    }

    public void setBrailleClose(String brailleClose) {
        this.brailleClose = brailleClose;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
