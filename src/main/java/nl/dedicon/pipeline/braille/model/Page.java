package nl.dedicon.pipeline.braille.model;

import lombok.Data;

/**
 *
 * @author Paul Rambags
 */
@Data
public class Page {
    
    private Integer fromPrintPageNumber;
    private Integer untilPrintPageNumber;
    private Integer pageNumber;
    
}
