package nl.dedicon.pipeline.braille.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Property services
 * 
 * @author Paul Rambags
 */
public class PropertyService {
    
    /**
     * Name of application.properties file:
     * environment variable PIPELINE2_DEDICON_APPLICATION_PROPERTIES can be used,
     * or system property org.daisy.pipeline.dedicon.application.properties
     */
    private static final String APPLICATION_PROPERTIES = "org.daisy.pipeline.dedicon.application.properties";

    private static final Logger LOGGER = LoggerFactory.getLogger(PropertyService.class);
    
    private static Properties properties = null;
    
    /**
     * Get a property from the application.properties file.
     * The location of the application.properties file is determined by
     * environment variable PIPELINE2_DEDICON_APPLICATION_PROPERTIES
     * or system property org.daisy.pipeline.dedicon.application.properties
     * 
     * @param key property
     * @param defaultValue default value, if property is not available
     * @return the value of the property, or the default value if the property is not available
     */
    public static String getProperty(String key, String defaultValue) {
        if (properties == null) {
            properties = loadProperties();
        }
        return StringUtils.defaultIfBlank(properties.getProperty(key), defaultValue);
    }
    
    private static Properties loadProperties() {
        Properties result = new Properties();
        String applicationPropertiesLocation = org.daisy.common.properties.Properties.getProperty(APPLICATION_PROPERTIES);
        if (StringUtils.isNotBlank(applicationPropertiesLocation)) {
            File applicationPropertiesFile = new File(applicationPropertiesLocation);
            if (applicationPropertiesFile.isFile()) {
                try {
                    result.load(new FileInputStream(applicationPropertiesFile));
                    String message = String.format("Loaded application properties from location %s", applicationPropertiesLocation);
                    LOGGER.info(message);
                } catch (IOException ex) {
                    String message = String.format("Failed to load application properties from location %s", applicationPropertiesLocation);
                    LOGGER.error(message, ex);
                }
            } else {
                String message = String.format("Couldn't find application properties location %s", applicationPropertiesLocation);
                LOGGER.warn(message);
            }
        } else {
            String message = "No application properties location supplied. Using default properties.";
            LOGGER.info(message);
        }
        return result;
    }

}
