<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs" version="2.0"
                xpath-default-namespace="http://www.daisy.org/z3986/2005/dtbook/"
                xmlns="http://www.daisy.org/z3986/2005/dtbook/">

    <xsl:output indent="no"/>
    <xsl:param name="move-print-cover-to-first-volume" required="no" select="'true'"/>
    <xsl:param name="move-print-colophon-to-last-volume" required="no" select="'true'"/>

    <!-- Add rearmatter if it does not exist -->
    <xsl:template match="book[not(rearmatter)]">
        <xsl:copy>
            <xsl:apply-templates/>
            <rearmatter>
                <xsl:if test="$move-print-colophon-to-last-volume='true'">
                    <xsl:copy-of select="//level1[@class='colophon']"/>
                </xsl:if>
                <xsl:call-template name="generate-colophon-page"/> 
            </rearmatter>
        </xsl:copy>
    </xsl:template>

    <!-- Insert generated title page template after doctitle or docauthor -->
    <xsl:template match="frontmatter/*[self::doctitle or self::docauthor][last()]" priority="5">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
        <xsl:call-template name="generate-title-page"/>                
    </xsl:template>

    <!-- Insert print and generated colophon template: after last item in rearmatter -->
    <xsl:template match="rearmatter">
        <xsl:copy>
            <xsl:apply-templates/>
            <xsl:if test="$move-print-colophon-to-last-volume='true'">
                <xsl:copy-of select="//level1[@class='colophon']"/>
            </xsl:if>
            <xsl:call-template name="generate-colophon-page"/> 
        </xsl:copy>
    </xsl:template>

    <!-- Identity template -->
    <xsl:template match="node()|@*">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*"/>
        </xsl:copy>
    </xsl:template>

    <!--
        Removes the print colophon.
        The print colophon is moved into the rearmatter.
    -->
    <xsl:template match="//level1[@class='colophon']">
        <xsl:if test="not($move-print-colophon-to-last-volume='true')">
            <xsl:copy>
                <xsl:apply-templates select="node()|@*"/>
            </xsl:copy>
        </xsl:if>
    </xsl:template>

    <!--
        Template: generate-title-page
        Inserts the generated title page for both AL and SV books.
    -->
    <xsl:template name="generate-title-page">
        <xsl:variable name="isbn" select="//meta[@name eq 'dc:Source']/@content"/>
        <level1 id="generated-title-page" class="other">
            <p id="generated-identifier">
                <xsl:value-of select="//meta[@name eq 'dc:Identifier']/@content"/>
            </p>
            <p id="generated-doctitle">
                <xsl:copy-of select="//doctitle/node()"/>
            </p>
            <p id="generated-docauthors">
                <xsl:value-of select="string-join(//docauthor,', ')"/>
            </p>
            <p id="generated-isbn">
                <xsl:if test="matches($isbn, '^[0-9-]{10,}$')">
                    ISBN: <xsl:value-of select="$isbn"/>
                </xsl:if>
            </p>
            <!--
                If the number of lines of this generated title page footer
                changes, also variable vertical-position-title-page-footer in
                dtbook-to-pef.xpl must be adjusted.
            -->
            <div id="generated-title-page-footer">
                <p>Dedicon</p>
                <p id="generated-volume-count">
                    Band <span class="placeholder-current-volume"/> van <span class="placeholder-total-volumes"/>
                </p>
            </div>
        </level1>
    </xsl:template>

    <!--
        Template: generate-colophon-page
        Inserts the generated colophon page for both AL and SV books.
    -->
    <xsl:template name="generate-colophon-page">
        <xsl:choose>
            <xsl:when test="//meta[@name eq 'prod:docType']/@content eq 'ro'">
                <!-- AL -->
                <level1 id="generated-colophon-page" class="other">
                    <h1>Colofon Dedicon</h1>
                    <p>Deze aangepaste leesvorm is uitsluitend bestemd voor
                        eigen gebruik door mensen met een leesbeperking. Hij wordt
                        uitgeleend door Bibliotheekservice Passend Lezen of de lokale
                        bibliotheek. De intellectuele eigendomsrechten op deze
                        aangepaste leesvorm berusten bij de Koninklijke Bibliotheek en de
                        rechthebbenden van het oorspronkelijke werk. Productie en
                        distributie vinden plaats op basis van artikel 15j en 15c van de
                        Nederlandse Auteurswet en conform de Regeling Toegankelijke
                        Lectuur voor mensen met een leesbeperking. Kopiëren, uitlenen of
                        doorverkopen aan anderen is niet toegestaan. Deze aangepaste
                        leesvorm is in <xsl:value-of select="year-from-date(current-date())"/>
                        geproduceerd door Stichting Dedicon.</p>
                </level1>
            </xsl:when>
            <xsl:otherwise>
                <!-- SV -->
                <!--
                    In the past, meta data prod:colophon was used to distinguish among
                    three different colophons for SV (Educatief)
                    <xsl:variable name="colophon" select="//meta[@name eq 'prod:colophon']/@content"/>
                -->
                <level1 id="generated-colophon-page" class="other">
                    <h1>Colofon Dedicon</h1>
                    <p>Deze aangepaste
                        leesvorm is verzorgd door Stichting Dedicon. Het werk is uitsluitend
                        bestemd voor eigen gebruik door mensen met een leesbeperking.
                        Het werk is eigendom van Stichting Dedicon en mag niet worden
                        vermenigvuldigd of aan derden worden uitgeleend of doorverkocht.
                        De intellectuele eigendomsrechten op deze aangepaste leesvorm
                        berusten bij Stichting Dedicon en de rechthebbenden van het
                        oorspronkelijke werk. Productie en distributie vinden plaats op
                        basis van artikel 15j en 15c van de Nederlandse Auteurswet en
                        conform de Regeling Toegankelijke Lectuur voor mensen met een
                        leesbeperking. Voor opmerkingen omtrent de kwaliteit van dit boek
                        of vragen over het gebruik ervan kan contact worden opgenomen
                        met Stichting Dedicon. Voor contactgegevens zie www.dedicon.nl.</p>
                </level1>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>
