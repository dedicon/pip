<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step type="dedicon:message"
    xmlns:p="http://www.w3.org/ns/xproc"
    xmlns:px="http://www.daisy.org/ns/pipeline/xproc"
    xmlns:dedicon="http://www.dedicon.nl"
    version="1.0">
    
    <p:input port="source" sequence="false" primary="true"/>
    <p:option name="severity" required="false"/>                <!-- one of either: WARN, INFO, DEBUG. Defaults to "INFO". Use px:error to throw errors. -->
    <p:option name="message" required="true"/>                  <!-- message to be logged.  -->
    <p:option name="reset" required="false"/>
    <p:output port="result" sequence="false" primary="true"/>

</p:declare-step>
