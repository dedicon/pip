<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step type="dedicon:symbols-list"
    xmlns:p="http://www.w3.org/ns/xproc"
    xmlns:px="http://www.daisy.org/ns/pipeline/xproc"
    xmlns:dedicon="http://www.dedicon.nl"
    version="1.0">
    
    <p:input port="source" sequence="false" primary="true"/>
    <p:option name="symbols-code" required="false"/>
    <p:option name="symbols-code-default" required="true"/>
    <p:option name="symbols-list-header" required="false"/>
    <p:output port="result" sequence="false" primary="true"/>
    
</p:declare-step>
