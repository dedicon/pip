<?xml version="1.0" encoding="UTF-8"?>
<p:library xmlns:p="http://www.w3.org/ns/xproc" xmlns:c="http://www.w3.org/ns/xproc-step" version="1.0">
    
    <p:import href="add-boilerplate.xpl"/>
    <p:import href="brl-must-start-with-newline.xpl"/>
    <p:import href="brl-replace-double-form-feeds.xpl"/>
    <p:import href="brl-uris.xpl"/>
    <p:import href="emphasis.xpl"/>
    <p:import href="empty-back-cover.xpl"/>
    <p:import href="flatten-sidebars.xpl"/>
    <p:import href="jacket.xpl"/>
    <p:import href="message.xpl"/>
    <p:import href="metadata.xpl"/>
    <p:import href="modify-preview.xpl"/>
    <p:import href="move-notes.xpl"/>
    <p:import href="processing-instructions.xpl"/>
    <p:import href="remove-print-toc.xpl"/>
    <p:import href="src-backslash-fix.xpl"/>
    <p:import href="symbols-list.xpl"/>
    
</p:library>
