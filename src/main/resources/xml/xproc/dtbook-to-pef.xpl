<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step type="dedicon:dtbook-to-pef" version="1.0"
                xmlns:dedicon="http://www.dedicon.nl"
                xmlns:p="http://www.w3.org/ns/xproc"
                xmlns:px="http://www.daisy.org/ns/pipeline/xproc"
                xmlns:dtb="http://www.daisy.org/z3986/2005/dtbook/"
                xmlns:pef="http://www.daisy.org/ns/2008/pef"
                xmlns:c="http://www.w3.org/ns/xproc-step"
                exclude-inline-prefixes="#all"
                name="main">

    <p:documentation xmlns="http://www.w3.org/1999/xhtml">
        <h1 px:role="name">DTBook to PEF (Dedicon)</h1>
        <p px:role="desc">Transforms a DTBook (DAISY 3 XML) document into a PEF.</p>
    </p:documentation>
    
    <p:input port="source" primary="true" px:name="source" px:media-type="application/x-dtbook+xml"/>
    
    <p:option name="stylesheet" select="'http://www.dedicon.nl/pipeline/modules/braille/default.scss'"/>
    <p:option name="include-preview" select="'true'"/>
    <p:option name="include-brf" select="'true'"/>
    <p:option name="include-metadata" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Include metadata file</h2>
        </p:documentation>
    </p:option>
    <p:option name="metadata-date" select="''"/>
    <p:option name="include-obfl" select="'false'"/>
    <!--
        These options have been disabled because of a bug in pipeline-webui.
        It seems impossible to enter a valid query string in pipeline-webui.
        See variable file-format below.
    -->
    <!--<p:option name="ascii-file-format" select="'(table:&quot;com_braillo.BrailloTableProvider.TableType.BRAILLO_6DOT_031_01&quot;)(line-breaks:dos)(pad:both)(charset:&quot;IBM00858&quot;)(file-extension:&quot;.brl&quot;)'"/>-->
    <!--<p:option name="ascii-table"/>-->
    <p:option name="add-boilerplate" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Add boilerplate text</h2>
            <p px:role="desc">When enabled, and when the input has a `docauthor` element, will insert boilerplate text such as a title page.</p>
        </p:documentation>
    </p:option>
    <p:option name="page-width" select="'-1'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name" px:inherit="prepend"/>
            <p px:role="desc" px:inherit="prepend" xml:space="preserve">Use `-1` to compute this from metadata.</p>
        </p:documentation>
    </p:option>
    <p:option name="page-height" select="'-1'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name" px:inherit="prepend"/>
            <p px:role="desc" px:inherit="prepend" xml:space="preserve">Use `-1` to compute this from metadata.</p>
        </p:documentation>
    </p:option>
    <p:option name="printing" select="'duplex'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_printing
        -->
        <!-- @todo 
            In case of 'simplex-form-feeds', the PEF should be modified such that every section with an odd number of pages gets an extra empty page
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>simplex</value>
                    <documentation>Simplex</documentation>
                    <value>simplex-form-feeds</value>
                    <documentation>Simplex with extra form feeds</documentation>
                    <value>duplex</value>
                    <documentation>Duplex</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Page layout: Printing</h2>
            <p px:role="desc" xml:space="preserve">Option `Simplex with extra form feeds` adds extra form feeds to the braille files to enforce single sided printing.</p>
        </p:documentation>
    </p:option>
    <p:option name="hyphenation" select="'from-meta'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_hyphenation
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>auto</value>
                    <documentation>Hyphenate words</documentation>
                    <value>manual</value>
                    <documentation>Only hyphenate words where there are soft hyphens (U+00AD)</documentation>
                    <value>none</value>
                    <documentation>Never hyphenate words</documentation>
                    <value>from-meta</value>
                    <documentation>Use value from metadata field</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
    </p:option>
    <p:option name="line-spacing" select="'single'"/>
    <p:option name="capital-letters" select="'true'"/>
    <p:option name="include-captions" select="'true'"/>
    <p:option name="include-images" select="'false'"/> <!-- displays the alt text -->
    <p:option name="include-image-groups" select="'true'"/>
    <p:option name="include-line-groups" select="'true'"/>
    <p:option name="include-production-notes" select="'true'"/>
    <p:option name="show-braille-page-numbers" select="'true'"/>
    <p:option name="show-print-page-numbers" select="'true'"/>
    <p:option name="show-inline-print-page-numbers" select="'from-meta'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_show_inline_print_page_numbers
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>true</value>
                    <documentation>Yes</documentation>
                    <value>false</value>
                    <documentation>No</documentation>
                    <value>from-meta</value>
                    <documentation>Use value from metadata field</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Page numbers: Show inline print page numbers</h2>
        </p:documentation>
    </p:option>
    <p:option name="force-braille-page-break" select="'false'"/>
    <p:option name="toc-depth" select="'6'"/>
    <p:option name="include-document-toc-in-last-volume" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Table of contents: Include document TOC</h2>
            <p px:role="desc" xml:space="preserve">Whether or not to include a document-level TOC in the last volume.</p>
        </p:documentation>
    </p:option>
    <p:option name="include-print-toc" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Table of contents: Include print TOC</h2>
            <p px:role="desc" xml:space="preserve">Whether or not to include a print TOC.</p>
        </p:documentation>
    </p:option>
    <p:option name="include-symbols-list" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Include symbols list</h2>
            <p px:role="desc" xml:space="preserve">When enabled, a list of symbols and their descriptions is included in the first volume.</p>
        </p:documentation>
    </p:option>
    <p:option name="symbols-code" select="''">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Symbols list location</h2>
            <p px:role="desc" xml:space="preserve">When empty, the default symbols list will be used.</p>
        </p:documentation>
    </p:option>
    <p:option name="symbols-list-header" required="false" px:type="string" select="'Symbolenlijst'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Symbols list header</h2>
        </p:documentation>
    </p:option>
    <p:option name="braille-notation-code" required="false" select="'nl'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_option_language
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>nl</value>
                    <documentation>Dutch</documentation>
                    <value>from-dtbook</value>
                    <documentation>Use value of dtbook element</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Translation/formatting of text: Braille notation code</h2>
            <p px:role="desc" xml:space="preserve">The Braille notation code used for interpolation.

If `Dutch` then capital signs, number signs etc. will be as in Dutch, while
hyphenation rules will follow the dtbook's language. If `Use value of dtbook element`
then the dtbook's language determines both interpolation and hyphenation.
</p>
        </p:documentation>
    </p:option>
    <p:option name="jacket-header" required="false" px:type="string" select="'(default)'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Volumes: Jacket header</h2>
            <p px:role="desc" xml:space="preserve">Header of the print cover.

If `(default)` then the value is computed from meta-data:
For non-educational books 'Flaptekst' will be used and for educational books no jacket header will be applied.
</p>
        </p:documentation>
    </p:option>
    <p:option name="move-print-cover" required="false" select="'from-meta'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_move_print_cover
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>no</value>
                    <documentation>No</documentation>
                    <value>front</value>
                    <documentation>To the front</documentation>
                    <value>rear</value>
                    <documentation>To the rear</documentation>
                    <value>from-meta</value>
                    <documentation>Use value from metadata field.</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Volumes: Move print cover</h2>
            <p px:role="desc" xml:space="preserve">Location to which the jacket will be moved.

When the input has a `level1` element with class `flap`, `jacket_copy` or `jacketcopy`, will move that cover to the indicated location.
In case this is computed from meta-data: If it is an educational book, the cover will be moved to the rear, otherwise to the front.
</p>
        </p:documentation>
    </p:option>
    <p:option name="move-print-colophon-to-last-volume" required="false" select="'from-meta'">
        <!--
            This option must be recorded in the POM under properties > expose-services
            as org.daisy.pipeline.datatypes.impl.Datatype_dedicon_dtbook_to_pef_move_print_colophon_to_last_volume
        -->
        <p:pipeinfo>
            <px:type>
                <choice>
                    <value>true</value>
                    <documentation>Yes</documentation>
                    <value>false</value>
                    <documentation>No</documentation>
                    <value>from-meta</value>
                    <documentation>Use value from metadata field</documentation>
                </choice>
            </px:type>
        </p:pipeinfo>
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Volumes: Move print colophon to last volume</h2>
            <p px:role="desc">When enabled, and when the input has a `level1` element with class `colophon`, will move that colophon to the last volume.</p>
        </p:documentation>
    </p:option>
    <p:option name="maximum-number-of-sheets" select="'-1'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name" px:inherit="prepend"/>
            <p px:role="desc" px:inherit="prepend" xml:space="preserve">Use `-1` to compute this from metadata.</p>
        </p:documentation>
    </p:option>
    <p:option name="volume-back-cover-empty" required="false" px:type="boolean" select="'true'">
        <p:documentation xmlns="http://www.w3.org/1999/xhtml">
            <h2 px:role="name">Volumes: The back cover must be empty</h2>
            <p px:role="desc" xml:space="preserve">Applies to duplex printing. When enabled, the last page of a printed volume will contain no text and can have pits only. If the number of pages of a volume is a 4-fold and the last page is not empty, an extra page will be appended.</p>
        </p:documentation>
    </p:option>
    <p:option name="pef-output-dir"/>
    <p:option name="output-dir" required="false" select="''"/>
    <p:option name="preview-output-dir" select="''"/>
    <p:option name="temp-dir" required="false" select="''"/>
    
    <p:variable name="file-format" px:type="string" select="'(table:&quot;com_braillo.BrailloTableProvider.TableType.BRAILLO_6DOT_031_01&quot;)(line-breaks:dos)(pad:both)(charset:&quot;IBM00858&quot;)(file-extension:&quot;.brl&quot;)'"/>
    <p:variable name="actual-page-width" select="
        if ($page-width = '-1')
        then if (//dtb:meta[@name='prod:docType']/@content='ro') then 28
             else if (//dtb:meta[@name='prod:docType']/@content='sv') then 33
             else 28
        else xs:integer($page-width)
    "/>
    <p:variable name="actual-page-height" select="
        if ($page-height='-1')
        then if (//dtb:meta[@name='prod:docType']/@content='ro') then 26
             else if (//dtb:meta[@name='prod:docType']/@content='sv') then 27 else 26
        else xs:integer($page-height)
    "/>
    <p:variable name="actual-jacket-header" select="
        if ($jacket-header = '(default)')
        then if (//dtb:meta[@name='prod:docType']/@content='sv') then ''
             else 'Flaptekst'
        else $jacket-header
    "/>
    <p:variable name="actual-move-print-cover" select="
        if ($move-print-cover = 'from-meta')
        then if (//dtb:meta[@name='prod:docType']/@content='sv') then 'rear'
             else 'front'
        else $move-print-cover
    "/>
    <!--
        The footer of the title page must be positioned at the bottom of the page.
        The code below uses that this footer consists of two lines of text.
        See generate-boilerplate.xsl, div id="generated-title-page-footer"
    -->
    <p:variable name="vertical-position-title-page-footer" select="
        let $title-page-footer-height := 2
        return $actual-page-height - $title-page-footer-height
        - ($title-page-footer-height - 1) * xs:integer($line-spacing = 'double')
        - xs:integer(xs:boolean($show-print-page-numbers) or xs:boolean($show-braille-page-numbers))
    "/>
    <!--
        Workaround for issue https://dedicon.atlassian.net/browse/PIP-151
        Currently, it is not possible to remove a resumed entry from the TOC
        if there is a <level1> without a <h1>. So we disable the resumed
        functionality in the TOC if the body has a <level1> without <h1>.
    -->
    <p:variable name="use-resumed-functionality-in-toc" px:type="boolean" select="empty(//dtb:bodymatter/dtb:level1[not(dtb:h1)])"/>
    <p:variable name="current-language" px:type="string" select="/dtb:dtbook/@xml:lang/string()"/>
    
    <p:import href="http://www.daisy.org/pipeline/modules/braille/dtbook-to-pef/library.xpl"/>
    <p:import href="http://www.daisy.org/pipeline/modules/braille/common-utils/library.xpl"/>
    <p:import href="http://www.daisy.org/pipeline/modules/braille/pef-utils/library.xpl"/>
    <p:import href="http://www.daisy.org/pipeline/modules/file-utils/library.xpl"/>
    <p:import href="http://www.daisy.org/pipeline/modules/common-utils/library.xpl"/>
    <p:import href="http://www.daisy.org/pipeline/modules/dtbook-utils/library.xpl"/>
    <p:import href="http://www.dedicon.nl/pipeline/modules/braille/library.xpl"/>

    <p:in-scope-names name="in-scope-names"/>
    <px:merge-parameters>
        <p:input port="source">
            <p:pipe port="result" step="in-scope-names"/>
        </p:input>
    </px:merge-parameters>
    <px:delete-parameters parameter-names="stylesheet
                                    symbols-code
                                    ascii-table
                                    include-brf
                                    include-preview
                                    include-obfl
                                    pef-output-dir
                                    output-dir
                                    preview-output-dir
                                    temp-dir"/>
    <px:add-parameters>

        <p:with-param name="toc-exclude-class" select="''"/>

        <p:with-param name="skip-margin-top-of-page" select="true()"/>
        <p:with-param name="page-width" select="string($actual-page-width)">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="page-height" select="string($actual-page-height)">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="maximum-number-of-sheets" select="if ($maximum-number-of-sheets='-1')
                               then if (//dtb:meta[@name='prod:docType']/@content='ro') then '35'
                                    else if (//dtb:meta[@name='prod:docType']/@content='sv') then '37' else '35'
                               else $maximum-number-of-sheets">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="hyphenation" select="if ($hyphenation='from-meta')
                               then if ((//dtb:meta[@name='prod:docHyphenate']/@content,'Y')[1]='Y')
                                    then 'auto'
                                    else 'manual'
                               else $hyphenation">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="show-inline-print-page-numbers" select="if ($show-inline-print-page-numbers='from-meta')
                               then //dtb:meta[@name='prod:docType']/@content='sv'
                               else $show-inline-print-page-numbers='true'">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="include-document-toc-in-last-volume" select="$include-document-toc-in-last-volume='true'">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="move-print-colophon-to-last-volume" select="if ($move-print-colophon-to-last-volume='from-meta')
                               then //dtb:meta[@name='prod:docType']/@content='sv'
                               else $move-print-colophon-to-last-volume='true'">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="duplex" select="$printing='duplex'">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="ascii-file-format" select="$file-format">
            <p:pipe step="main" port="source"/>
        </p:with-param>
        <p:with-param name="allow-volume-break-inside-leaf-section-factor" select="'9'"/>
        <p:with-param name="prefer-volume-break-before-higher-level-factor" select="'0'"/>
        <p:with-param name="notes-placement" select="'custom'"/>
    </px:add-parameters>
    <p:identity name="input-options"/>
    <p:sink/>
    
    <!-- =============== -->
    <!-- CREATE TEMP DIR -->
    <!-- =============== -->
    <px:tempdir name="temp-dir">
        <p:with-option name="href" select="if ($temp-dir!='') then $temp-dir else $pef-output-dir"/>
    </px:tempdir>
    <p:sink/>
    
    <!-- ==================== -->
    <!-- DTBOOK PREPROCESSING -->
    <!-- ==================== -->
    <dedicon:message reset="true" message="Start Dedicon pre-processing">
        <p:input port="source">
            <p:pipe step="main" port="source"/>
        </p:input>
    </dedicon:message>
    <dedicon:message message="Applying emphasis"/>
    <dedicon:emphasis/>
    <dedicon:message message="Applying processing instructions"/>
    <dedicon:processing-instructions/>
    <p:choose>
        <p:when test="$include-print-toc = 'false'">
            <dedicon:message message="Removing print TOC"/>
            <dedicon:remove-print-toc/>
        </p:when>
        <p:otherwise>
            <dedicon:message severity="TRACE" message="Keeping print TOC"/>
        </p:otherwise>
    </p:choose>
    <dedicon:message message="Applying fix for backslashes in attribute src"/>
    <dedicon:src-backslash-fix/>
    <!--    
    <dedicon:message message="Flattening sidebars"/>
    <dedicon:flatten-sidebars/>
    -->
    <dedicon:message message="Moving notes after their noteref"/>
    <dedicon:move-notes/>
    <p:choose>
        <p:when test="$add-boilerplate='true'">
            <dedicon:message message="Adding boilerplate"/>
            <dedicon:add-boilerplate>
                <p:input port="parameters">
                    <p:pipe step="input-options" port="result"/>
                </p:input>
            </dedicon:add-boilerplate>
        </p:when>
        <p:otherwise>
            <dedicon:message severity="TRACE" message="Skipping boilerplate"/>
        </p:otherwise>
    </p:choose>
    <p:choose>
        <p:when test="$actual-move-print-cover != 'no'">
            <dedicon:message message="Managing jacket"/>
            <dedicon:jacket>
                <p:with-option name="jacket-header" select="$actual-jacket-header"/>
                <p:with-option name="move-print-cover" select="$actual-move-print-cover"/>
            </dedicon:jacket>
        </p:when>
        <p:otherwise>
            <dedicon:message severity="TRACE" message="Skipping jacket"/>
        </p:otherwise>
    </p:choose>
    <p:choose>
        <p:when test="$include-symbols-list='true'">
            <dedicon:message message="Including symbols list"/>
            <dedicon:symbols-list>
                <p:with-option name="symbols-code" select="$symbols-code"/>
                <p:with-option name="symbols-code-default" select="'http://www.dedicon.nl/pipeline/modules/braille/symbols_code.xml'"/>
                <p:with-option name="symbols-list-header" select="$symbols-list-header"/>
            </dedicon:symbols-list>
        </p:when>
        <p:otherwise>
            <dedicon:message severity="TRACE" message="Skipping symbols list"/>
        </p:otherwise>
    </p:choose>
    <dedicon:message message="End Dedicon pre-processing"/>
    
    <!--
        FIXME: The pre-processing steps above set the base URI to a temporary file either (or the
        empty string if `useFile` would be set to false), which for some reason makes the conversion
        fail further down in the process. Setting the base URI to this dummy string works around the
        issue, but is not such a nice solution. Better would be make the pre-processing steps
        preserve the base URI of the input document.
    -->
    <px:set-base-uri base-uri="file:/irrelevant.xml"/>
        
    <!-- ============= -->
    <!-- DTBOOK TO PEF -->
    <!-- ============= -->
    <px:dtbook-load name="load"/>
    
    <px:dtbook-to-pef transform="(formatter:dotify)(type:literary)(dots-for-undefined-char:'⠢')"
                      name="convert">
        <p:input port="source.in-memory">
            <p:pipe step="load" port="result.in-memory"/>
        </p:input>
        <p:with-option name="temp-dir" select="string(/c:result)">
            <p:pipe step="temp-dir" port="result"/>
        </p:with-option>
        <p:with-option name="stylesheet" select="$stylesheet"/>
        <p:with-option name="include-obfl" select="$include-obfl"/>
        <p:input port="parameters">
            <p:pipe port="result" step="input-options"/>
        </p:input>
    </px:dtbook-to-pef>
    
    <p:group>
        <p:variable name="identifier" select="//dtb:meta[@name='dc:Identifier']/@content">
            <p:pipe step="main" port="source"/>
        </p:variable>
        <p:variable name="name" select="if ($identifier) then $identifier
                                        else replace(p:base-uri(/),'^.*/([^/]*)\.[^/\.]*$','$1')">
            <p:pipe step="main" port="source"/>
        </p:variable>
        <p:variable name="title" select="//dtb:meta[@name='dc:Title']/@content">
            <p:pipe step="main" port="source"/>
        </p:variable>
        
        <p:for-each>
            <!-- Make sure the PEF ends with an EMPTY BACK COVER -->
            <p:choose>
                <p:when test="$volume-back-cover-empty='true'">
                    <dedicon:message message="Volume back cover will be empty"/>
                    <dedicon:empty-back-cover>
                        <p:with-option name="xquery" select="resolve-uri('../../xquery/empty-back-cover.xql')"/>
                    </dedicon:empty-back-cover>
                </p:when>
                <p:otherwise>
                    <dedicon:message severity="TRACE" message="Skipping ''volume back cover will be empty''"/>
                </p:otherwise>
            </p:choose>

            <p:identity name="pef"/>

            <!-- ===== -->
            <!-- STORE -->
            <!-- ===== -->
            <p:group>
                <p:variable name="preview-href" select="
                    if ($include-preview='true' and $preview-output-dir!='')
                    then concat($preview-output-dir,'/',$name,'.pef.html')
                    else ''
                "/>
                <!--<p:variable name="brf-file-format" select="$ascii-file-format"/>-->
                <p:variable name="brf-file-format" select="$file-format"/>
                <p:variable name="brf-name-pattern" select="concat('p',$name,'_{}')"/>
                <p:variable name="brf-number-width" select="3"/>
                <p:variable name="brf-dir-href" select="
                    if ($include-brf='true')
                    then
                        if ($output-dir!='')
                        then $output-dir
                        else
                            if ($pef-output-dir!='')
                            then $pef-output-dir
                            else ''
                    else ''
                "/>

                <!-- Store PEF, BRLs -->
                <px:pef-store>
                    <p:with-option name="pef-href" select="concat($pef-output-dir,'/',$name,'.pef')"/>
                    <p:with-option name="preview-href" select="$preview-href"/>
                    <!-- <p:with-option name="preview-table" select="if ($ascii-table!='') then $ascii-table
                                                            else concat('(locale:',(/*/@xml:lang,'und')[1],')')"/> -->
                    <p:with-option name="output-dir" select="$brf-dir-href"/>
                    <p:with-option name="file-format" select="$brf-file-format"/>
                    <p:with-option name="name-pattern" select="$brf-name-pattern"/>
                    <p:with-option name="number-width" select="$brf-number-width"/>
                </px:pef-store>

                <!-- Make sure each BRL starts with a NEWLINE -->
                <dedicon:message message="Inserting newline in BRL files">
                    <p:input port="source">
                        <p:pipe port="result" step="pef"/>
                    </p:input>
                </dedicon:message>
                <dedicon:brl-uris name="brl-uris">
                    <p:with-option name="xquery" select="resolve-uri('../../xquery/brl-uris.xql')"/>
                    <p:with-option name="brf-dir-href" select="$brf-dir-href"/>
                    <p:with-option name="brf-file-format" select="$brf-file-format"/>
                    <p:with-option name="brf-name-pattern" select="$brf-name-pattern"/>
                    <p:with-option name="brf-number-width" select="$brf-number-width"/>
                </dedicon:brl-uris>
                <p:for-each>
                    <dedicon:brl-must-start-with-newline>
                        <p:with-option name="brl-uri" select="."/>
                    </dedicon:brl-must-start-with-newline>
                </p:for-each>

                <!-- Replace DOUBLE FORM FEEDS in BRL -->
                <p:choose>
                    <p:when test="$printing='simplex'">
                        <dedicon:message message="Replacing double form feeds in BRL files">
                            <p:input port="source">
                                <p:empty/>
                            </p:input>
                        </dedicon:message>
                        <p:for-each>
                            <p:iteration-source>
                                <p:pipe step="brl-uris" port="result"/>
                            </p:iteration-source>
                            <dedicon:brl-replace-double-form-feeds>
                                <p:with-option name="brl-uri" select="."/>
                            </dedicon:brl-replace-double-form-feeds>
                        </p:for-each>
                    </p:when>
                    <p:otherwise>
                        <p:sink>
                            <p:input port="source">
                                <p:empty/>
                            </p:input>
                        </p:sink>
                    </p:otherwise>
                </p:choose>

                <!-- Store METADATA -->
                <p:choose>
                    <p:when test="$include-metadata='true'">
                        <dedicon:message message="Storing metadata">
                            <p:input port="source">
                                <p:pipe port="result" step="pef"/>
                            </p:input>
                        </dedicon:message>
                        <dedicon:metadata>
                            <p:with-option name="xquery" select="resolve-uri('../../xquery/metadata.xql')"/>
                            <p:with-option name="identifier" select="$identifier"/>
                            <p:with-option name="brf-file-format" select="$brf-file-format"/>
                            <p:with-option name="brf-name-pattern" select="$brf-name-pattern"/>
                            <p:with-option name="brf-number-width" select="$brf-number-width"/>
                            <p:with-option name="optional-date" select="$metadata-date"/>
                        </dedicon:metadata>
                        <p:store>
                            <p:with-option name="href" select="concat($pef-output-dir, '/m', $name, '_001.xml')"/>
                        </p:store>
                    </p:when>
                    <p:otherwise>
                        <dedicon:message severity="TRACE" message="Skipping metadata">
                            <p:input port="source">
                                <p:empty/>
                            </p:input>
                        </dedicon:message>
                        <p:sink/>
                    </p:otherwise>
                </p:choose>

                <!-- Modify PREVIEW -->
                <p:choose>
                    <p:when test="$preview-href != ''">
                        <dedicon:message message="Modifying preview">
                            <p:input port="source">
                                <p:empty/>
                            </p:input>
                        </dedicon:message>
                        <dedicon:modify-preview>
                            <p:with-option name="preview-uri" select="$preview-href"/>
                        </dedicon:modify-preview>
                    </p:when>
                    <p:otherwise>
                        <p:sink>
                            <p:input port="source">
                                <p:empty/>
                            </p:input>
                        </p:sink>
                    </p:otherwise>
                </p:choose>
            </p:group>
        </p:for-each>
            
        <!-- Store OBFL -->
        <p:for-each>
            <p:iteration-source>
                <p:pipe step="convert" port="obfl"/>
            </p:iteration-source>
            <dedicon:message message="Storing OBFL"/>
            <p:store>
                <p:with-option name="href" select="concat($pef-output-dir,'/',$name,'.obfl')"/>
            </p:store>
        </p:for-each>
        
        <!-- Final message -->
        <!-- @todo let this be the last message -->
        <dedicon:message>
            <p:with-option name="message" select="concat('Finished ''', $title, '''')"/>
            <p:input port="source">
                <p:empty/>
            </p:input>
        </dedicon:message>
        <p:sink/>
    </p:group>

</p:declare-step>
