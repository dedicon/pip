<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step type="dedicon:brl-uris"
    xmlns:p="http://www.w3.org/ns/xproc"
    xmlns:px="http://www.daisy.org/ns/pipeline/xproc"
    xmlns:dedicon="http://www.dedicon.nl"
    version="1.0">
    
    <p:input port="source" sequence="false" primary="true"/>
    <p:option name="xquery" required="true"/>
    <p:option name="brf-dir-href" required="false"/>
    <p:option name="brf-file-format" required="false"/>
    <p:option name="brf-name-pattern" required="false"/>
    <p:option name="brf-number-width" required="false"/>
    <p:output port="result" sequence="true" primary="true"/>
    
</p:declare-step>
