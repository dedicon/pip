<?xml version="1.0" encoding="UTF-8"?>
<p:declare-step type="dedicon:empty-back-cover"
    xmlns:p="http://www.w3.org/ns/xproc"
    xmlns:px="http://www.daisy.org/ns/pipeline/xproc"
    xmlns:dedicon="http://www.dedicon.nl"
    version="1.0">
    
    <p:input port="source" sequence="false" primary="true"/>
    <p:option name="xquery" required="true"/>
    <p:output port="result" sequence="false" primary="true"/>

</p:declare-step>
