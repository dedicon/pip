declare default element namespace "http://www.daisy.org/ns/2008/pef";

declare function local:pef($pef as element(pef)) as element(pef) {
    <pef>
        { $pef/attribute() }
        {
            for $element in $pef/element()
            return

            if ($element/self::body)
            then local:body($element)
            else $element
        }
    </pef>
};

declare function local:body($body as element(body)) as element(body) {
    <body>
        { $body/attribute() }
        {
            for $element in $body/element()
            return

            if ($element/self::volume)
            then local:volume($element)
            else $element
        }
    </body>
};

declare function local:volume($volume as element(volume)) as element(volume) {
    (:
        the empty back cover is only relevant in duplex mode
    :)
    if ($volume/@duplex != 'true')
    then $volume
    else

    let $no-of-pages := local:count-pages($volume)
    return

    if (($no-of-pages mod 4) != 0 or local:last-page-is-empty($volume))
    then $volume
    else
  
    (:
        add an empty page
    :)
    <volume>
        { $volume/attribute() }
        {
            for $element in $volume/element()
            return

            if ($element/self::section and empty($element/following-sibling::section))
            then local:append-page($element)
            else $element
        }
    </volume>
};

declare function local:count-pages($volume as element(volume)) as xs:integer {
    sum (
        for $section in $volume/section
        let $no-of-pages := count($section/page)
        (:
            add 1 if the number of pages is odd and there is a following section
        :)
        let $extra-page :=
            if ($section/following-sibling::section)
            then $no-of-pages mod 2
            else 0
        return $no-of-pages + $extra-page
    )
};

declare function local:last-page-is-empty($volume as element(volume)) as xs:boolean {
    let $last-section := $volume/section[last()]
    let $last-page := $last-section/page[last()]
    let $text := normalize-space(string-join($last-page/row/text(), ''))
    return string-length($text) = 0
};

declare function local:append-page($section as element(section)) as element(section) {
    <section>
        { $section/attribute() }
        { $section/element() }
        <page/>
    </section>
};


(:
    main
:)
document {
    local:pef(./pef)
}
