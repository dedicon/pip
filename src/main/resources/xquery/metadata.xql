declare namespace pef = "http://www.daisy.org/ns/2008/pef";

declare variable $identifier external;
declare variable $brf-name-pattern external;
declare variable $brf-number-width external;
declare variable $brf-file-extension external;
declare variable $optional-date external;

let $date :=
    if (string-length($optional-date) = 0)
    then
        let $current-date := current-date()
        return concat(day-from-date($current-date), '-', month-from-date($current-date), '-', year-from-date($current-date))
    else $optional-date
return

<document>
    <lois_id>{ $identifier }</lois_id>
    {
        let $volumes := /pef:pef/pef:body/pef:volume
        let $volumes-count := count($volumes)
        let $volumes-without-cp :=
            for $volume-number in 1 to $volumes-count
            let $volume := $volumes[$volume-number]
            let $duplex := xs:boolean($volume/@duplex)

            let $volume-number-string := string($volume-number)
            let $brf-number :=
                if (string-length($volume-number-string) >= $brf-number-width)
                then $volume-number-string
                else
                    let $with-zeros := concat('0000000000000000', $volume-number-string)
                    return substring($with-zeros, string-length($with-zeros) - $brf-number-width + 1)
            let $filename := concat(replace($brf-name-pattern, '\{\}', $brf-number), $brf-file-extension)

            let $page-count :=
                if ($duplex)
                then sum (
                        for $section in $volume/pef:section
                        let $count := count($section/pef:page)
                        return $count + ($count mod 2)
                    )
                else
                    count($volume/pef:section/pef:page)

            let $last := if ($volume-number = $volumes-count) then 'Y' else 'N'
            return

            <volume>
                <filename>{ $filename }</filename>
                <vtype>br</vtype>
                <volumenumber>{ $volume-number }</volumenumber>
                <fromip/>
                <tillip/>
                <ippages/>
                <fromcp/>
                <tillcp/>
                <amount>{ $page-count }</amount>
                <last>{ $last }</last>
                <vreadydate>{ $date }</vreadydate>
            </volume>

        let $volumes-with-cp :=
            for $volume-number in 1 to $volumes-count
            let $volume := $volumes-without-cp[$volume-number]
            let $preceding-volumes := $volumes-without-cp[position() < $volume-number]
            let $sum-preceding-amounts := sum($preceding-volumes/amount)
            let $fromcp := $sum-preceding-amounts + 1
            let $tillcp := $sum-preceding-amounts + $volume/amount
            return

            <volume>
            {
                for $element in $volume/element()
                return

                if (exists($element/self::fromcp)) then <fromcp>{ $fromcp }</fromcp> else
                if (exists($element/self::tillcp)) then <tillcp>{ $tillcp }</tillcp> else
                $element
            }
            </volume>
        return

        $volumes-with-cp
    }
</document>
