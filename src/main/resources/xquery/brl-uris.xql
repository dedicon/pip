declare namespace pef = "http://www.daisy.org/ns/2008/pef";

declare variable $brf-dir-href external;
declare variable $brf-name-pattern external;
declare variable $brf-number-width external;
declare variable $brf-file-extension external;

let $current-date := current-date()
let $date := concat(day-from-date($current-date), '-', month-from-date($current-date), '-', year-from-date($current-date))

let $volumes := /pef:pef/pef:body/pef:volume
let $volumes-count := count($volumes)
for $volume-number in 1 to $volumes-count
let $volume := $volumes[$volume-number]

let $volume-number-string := string($volume-number)
let $brf-number :=
    if (string-length($volume-number-string) >= $brf-number-width)
    then $volume-number-string
    else
        let $with-zeros := concat('0000000000000000', $volume-number-string)
        return substring($with-zeros, string-length($with-zeros) - $brf-number-width + 1)
let $uri := concat($brf-dir-href, '/', replace($brf-name-pattern, '\{\}', $brf-number), $brf-file-extension)
return 

<uri>{ $uri }</uri>
